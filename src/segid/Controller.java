package segid;

import javafx.event.ActionEvent;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Controller {

    public TextField inputTxt,averageTxt,longestSegmentTxt,minLengthTxt;
    public TextArea allMaxSegTxt;
    public void sampleInput(ActionEvent actionEvent) {
        inputTxt.setText("7 7 1 4 5 7 5 6 6 1 2 5 4 7 6 1 2 3 5 7 8");
        averageTxt.setText("5");
    }

    public void random(ActionEvent actionEvent) {
        Random rand = new Random();
        int size = 10;
        int average = rand.nextInt(10);
        averageTxt.setText(average+"");
        minLengthTxt.setText(rand.nextInt(10)+ 1 + "");
        int[] input = new int[size];
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<size;i++){
            input[i] = average + rand.nextInt(10) - 5;
            sb.append(input[i]+" ");
        }
        inputTxt.setText(sb.toString());
    }

    public void allMaxSegs(ActionEvent actionEvent){
        int[] input = Arrays.stream(inputTxt.getText().split(" ")).map(String::trim).mapToInt(Integer::parseInt).toArray();
        Algorithm al = new Algorithm(input, Integer.parseInt(averageTxt.getText().trim()));
        ArrayList<ArrayList<Integer>> segments = al.getAllMaxSegments( Integer.parseInt(minLengthTxt.getText().trim()));
        final int SIZE = segments.size();
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<SIZE;i++)
        {
            ArrayList<Integer> segment = segments.get(i);
            int segSize = segment.size();
            for (int j=0;j<segSize;j++){
                sb.append(segment.get(j)+" ");
            }
            sb.append("\n");
        }
        allMaxSegTxt.setText(sb.toString());
    }

    public void longestSegment(ActionEvent actionEvent){
        int[] input = Arrays.stream(inputTxt.getText().split(" ")).map(String::trim).mapToInt(Integer::parseInt).toArray();
        Algorithm al = new Algorithm(input, Integer.parseInt(averageTxt.getText().trim()));
        int [] longestSegment = al.getLongestSegment();
        if (longestSegment!= null){
            final int SIZE = longestSegment.length;
            StringBuilder sb = new StringBuilder();
            int sum = 0;
            for (int i=0;i<SIZE;i++){
                int v = longestSegment[i];
                sb.append(v+" ");
                sum +=v;
            }
            double average = 1.0 * sum/SIZE;
            DecimalFormat df=new DecimalFormat("0.00");
            longestSegmentTxt.setText(sb.toString()+" A:[ "+df.format(average)+" ] L:[ "+SIZE+" ]");
        }else {
            longestSegmentTxt.setText("There is no such segment");
        }

    }
}
