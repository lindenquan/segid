package segid;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Linden on 28/01/2017.
 */
public class Algorithm {
    private int[] input;
    private int SIZE;
    private int average;
    private int minLength;
    private int[] sum;
    private int[] max;
    private int[] msp;
    private int[] mspV2;
    private int[] ms;
    private int[] l;
    private int[] longestSegment;

    private final int SEG_VOID = 0;
    private final int SEG_START = 1;
    private final int SEG_END = 2;
    private final int NO_LFET_PARTNER = -1;
    private final int NO_LFET_SEGMENT = -1;

    Algorithm(int[] input, int average) {
        this.input = Arrays.copyOf(input, input.length);
        this.average = average;
        this.SIZE = input.length;

    }

    private int sum(int i, int j) {
        if (i == 0)
            return sum[j];
        return sum[j] - sum[i - 1];
    }

    private void printArray(int[] a) {
        Arrays.stream(a).forEach(i -> System.out.print(i + " "));
        System.out.println();
    }

    private void preprocess() {
        input = Arrays.stream(input).map(i -> i - average).toArray();
        sum = new int[SIZE];
        sum[0] = input[0];
        for (int i = 1; i < SIZE; i++) {
            sum[i] = sum[i - 1] + input[i];
        }
        System.out.print("sum: ");
        printArray(sum);
        max = new int[SIZE];
        msp = new int[SIZE];
        max[SIZE - 1] = sum[SIZE - 1];
        msp[SIZE - 1] = SIZE - 1;


        for (int i = SIZE - 2; i > -1; i--) {
            int temp1 = sum[i];
            int temp2 = max[i + 1];
            if (temp1 > temp2) {
                max[i] = temp1;
                msp[i] = i;
            } else {
                max[i] = temp2;
                msp[i] = msp[i + 1];
            }
        }
        System.out.print("max: ");
        printArray(max);

        System.out.print("msp: ");
        printArray(msp);

        ms = new int[SIZE];
        mspV2 = new int[SIZE];

        for (int i = 0; i < SIZE; i++) {
            if (i == 0)
                ms[i] = max[i];
            else
                ms[i] = max[i] - sum[i - 1];
        }
        System.out.print("sm: ");
        printArray(ms);

        int j = SIZE - 1;
        for (int i = SIZE - 1; i > -1; i--) {
            while (true) {
                if (sum(i, j) == ms[i]) {
                    mspV2[i] = j;
                    break;
                } else {
                    j--;
                }
            }
        }
        System.out.print("mspV2: ");
        printArray(mspV2);
    }

    private void mainprocess() {
        l = new int[SIZE];
        int i1, j = 0;
        for (int i = 0; i < SIZE; j++) {
            //int j = Math.max(l[i1] + 1, i);
            while (j < SIZE) {
                if (sum(i, j) >= 0) {
                    if (j == SIZE - 1) {
                        l[i] = j;
                        return;
                    }
                    if (sum(i, msp[j + 1]) < 0) {
                        l[i] = j;
                        break;
                    }
                } else {
                    if (j == SIZE - 1) {
                        l[i] = -1;
                        return;
                    }
                    if (sum(i, msp[j + 1]) < 0) {
                        l[i] = -1;
                        break;
                    }
                }
                j = msp[j + 1];
            }
            i1 = i;
            do {
                i++;
                if (i == SIZE) break;
            } while (sum(i1, i) >= 0);
        }
    }

    private void mainprocess1() {
        l = new int[SIZE];
        int i1 = 0;
        l[0] = -1;
        for (int i = 0; i < SIZE; ) {
            int j = Math.max(l[i1] + 1, i);
            while (j < SIZE) {
                if (sum(i, msp[j]) < 0) {
                    if (j - 1 < 0) {
                        l[i] = -1;
                    } else {
                        l[i] = msp[j - 1];
                    }
                    i1 = i;
                    break;
                }
                if (j == SIZE - 1) {
                    l[i] = SIZE - 1;
                    return;
                }
                j = msp[j] + 1;
            }
            do {
                i++;
                if (i == SIZE) break;
            } while (sum(i1, i) >= 0);
        }
    }

    private void postprocess() {
        int longestLength = 0;
        int startI = -1;
        for (int i = 0; i < SIZE; i++) {
            int temp = l[i] - i + 1;
            if (temp > longestLength) {
                longestLength = temp;
                startI = i;
            }
        }

        if (startI == -1 || sum(startI, startI + longestLength - 1) < 0) {
            return;
        }
        if (longestLength > 0) {
            longestSegment = new int[longestLength];
            for (int i = 0; i < longestLength; i++) {
                longestSegment[i] = input[startI + i] + average;
            }
        }
    }

    public int[] getLongestSegment() {
        System.out.println("----------------------------------");
        System.out.print("input: ");
        printArray(input);
        preprocess();
        mainprocess();
        postprocess();
        return longestSegment;
    }

    class Segment {
        int start;
        int end;
        Segment lp;

        Segment(int s, int e) {
            start = s;
            end = e;
        }
    }

    private int findSegment(int startIndex) {
        int lastIndex = startIndex;
        for (int i = startIndex; i < SIZE; i++) {
            if (input[i] >= 0) {
                lastIndex = i;
            } else {
                break;
            }
        }
        return lastIndex;
    }

    class Int {
        int value;

        Int(int v) {
            value = v;
        }
    }

    public ArrayList<ArrayList<Integer>> getAllMaxSegments(int minLength) {
        this.minLength = minLength;
        preprocess();
        // main process
        int[] allMaxSegments = new int[SIZE];
        int[] leftPartnerK = new int[SIZE];
        int[] leftPartnerL = new int[SIZE];
        int[] leftK = new int[SIZE];
        int[] leftL = new int[SIZE];

        Int k = new Int(-1), l = new Int(-1);
        Int i = new Int(-1), j = new Int(-1);

        for (int index = 0; index < SIZE; ) {
            if (input[index] >= 0) {

                int lastIndex = findSegment(index);
                i.value = index;
                j.value = lastIndex;
                merge(k, l, i, j, leftPartnerK, leftPartnerL, leftK, leftL, allMaxSegments);
                index = lastIndex + 2;
            } else {
                index++;
            }
        }

        return allMaxPostProcess(allMaxSegments);
    }

    private void merge(Int k, Int l, Int i, Int j, int[] leftPartnerK, int[] leftPartnerL, int[] leftK, int[] leftL, int[] allMaxSegments) {
        boolean init = false;
        if (k.value == -1) {
            // this is first segment
            init = true;
            leftK[i.value] = NO_LFET_SEGMENT;
            leftL[i.value] = NO_LFET_SEGMENT;
        } else {
            // try to merge
            // find left partner
            while (sum(k.value, i.value - 1) < 0) {
                l.value = leftPartnerL[k.value];
                k.value = leftPartnerK[k.value];
                if (k.value == NO_LFET_PARTNER) {
                    break;
                }
            }
            if (k.value == NO_LFET_PARTNER) {
                init = true;
                leftK[i.value] = k.value;
                leftL[i.value] = l.value;
            } else {
                if (sum(l.value + 1, j.value) >= 0) {
                    // merge
                    allMaxSegments[k.value] = SEG_START;
                    allMaxSegments[j.value] = SEG_END;

                    leftK[i.value] = 0;
                    leftL[i.value] = 0;
                    for (int segIndex = k.value + 1; segIndex < j.value; segIndex++) {
                        allMaxSegments[segIndex] = 0;
                    }
                    i.value = k.value;
                    l.value = leftL[k.value];
                    k.value = leftK[k.value];

                    merge(k, l, i, j, leftPartnerK, leftPartnerL, leftK, leftL, allMaxSegments);
                } else {
                    init = true;
                    leftK[i.value] = k.value;
                    leftL[i.value] = l.value;
                }
            }

        }

        if (init) {
            allMaxSegments[i.value] = SEG_START;
            allMaxSegments[j.value] = SEG_END;
            leftPartnerK[i.value] = NO_LFET_PARTNER;
            leftPartnerL[i.value] = NO_LFET_PARTNER;
            k.value = i.value;
            l.value = j.value;
        }
    }


    private ArrayList<ArrayList<Integer>> allMaxPostProcess(int[] allMaxSegments) {
        ArrayList<ArrayList<Integer>> allSegments = new ArrayList();
        boolean isExpectingEnd = false;
        boolean isAdd = false;
        ArrayList<Integer> segment = null;
        for (int i = 0; i < SIZE; i++) {
            int value = allMaxSegments[i];
            switch (value) {
                case SEG_VOID:
                    if (isAdd) {
                        segment.add(input[i] + average);
                    }
                    break;
                case SEG_START:
                    segment = new ArrayList();
                    segment.add(input[i] + average);
                    allSegments.add(segment);
                    isExpectingEnd = true;
                    isAdd = true;
                    break;
                case SEG_END:
                    if (isExpectingEnd) {
                        isExpectingEnd = false;
                        isAdd = false;
                        segment.add(input[i] + average);
                    } else {
                        segment = new ArrayList();
                        segment.add(input[i] + average);
                        allSegments.add(segment);
                    }
                    break;
            }
        }
        int size = allSegments.size();
        for (int i = 0; i < size; ) {
            if (allSegments.get(i).size() < minLength) {
                allSegments.remove(i);
                size--;
            } else {
                i++;
            }
        }
        return allSegments;
    }

}
